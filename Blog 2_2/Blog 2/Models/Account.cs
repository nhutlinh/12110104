﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        public int ID { set; get; }
        [Required]
        [StringLength(32, ErrorMessage = "So ki tu phai tu 8 den 32", MinimumLength = 8)]
        public String PassWord { set; get; }
        public String Email { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}