namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PassWord = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.BaiViet", "AccountID", c => c.Int(nullable: false));
            AddForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts", "ID", cascadeDelete: true);
            CreateIndex("dbo.BaiViet", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts");
            DropColumn("dbo.BaiViet", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
