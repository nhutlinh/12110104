namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "PassWord", c => c.String(nullable: false, maxLength: 32));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "PassWord", c => c.String());
        }
    }
}
