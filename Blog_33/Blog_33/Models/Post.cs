﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_33.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}