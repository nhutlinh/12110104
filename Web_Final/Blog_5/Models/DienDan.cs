﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class DienDan
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MaBai { set; get; }
        public String TenBai { set; get; }
        public int UserId { set; get; }
        public String UserName { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime NgayDang { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}