﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class TrangChu
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
    }
}