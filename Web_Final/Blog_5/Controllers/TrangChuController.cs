﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class TrangChuController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /TrangChu/

        public ActionResult Index()
        {
            return View(db.TrangChus.ToList());
        }

        //
        // GET: /TrangChu/Details/5

        public ActionResult Details(int id = 0)
        {
            TrangChu trangchu = db.TrangChus.Find(id);
            if (trangchu == null)
            {
                return HttpNotFound();
            }
            return View(trangchu);
        }

        //
        // GET: /TrangChu/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TrangChu/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TrangChu trangchu)
        {
            if (ModelState.IsValid)
            {
                db.TrangChus.Add(trangchu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trangchu);
        }

        //
        // GET: /TrangChu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TrangChu trangchu = db.TrangChus.Find(id);
            if (trangchu == null)
            {
                return HttpNotFound();
            }
            return View(trangchu);
        }

        //
        // POST: /TrangChu/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TrangChu trangchu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trangchu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trangchu);
        }

        //
        // GET: /TrangChu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TrangChu trangchu = db.TrangChus.Find(id);
            if (trangchu == null)
            {
                return HttpNotFound();
            }
            return View(trangchu);
        }

        //
        // POST: /TrangChu/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrangChu trangchu = db.TrangChus.Find(id);
            db.TrangChus.Remove(trangchu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}