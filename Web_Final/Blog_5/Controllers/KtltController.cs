﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class KtltController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Ktlt/

        public ActionResult Index()
        {
            return View(db.Ktlts.ToList());
        }

        //
        // GET: /Ktlt/Details/5

        public ActionResult Details(int id = 0)
        {
            Ktlt ktlt = db.Ktlts.Find(id);
            if (ktlt == null)
            {
                return HttpNotFound();
            }
            return View(ktlt);
        }

        //
        // GET: /Ktlt/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Ktlt/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ktlt ktlt)
        {
            if (ModelState.IsValid)
            {
                db.Ktlts.Add(ktlt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ktlt);
        }

        //
        // GET: /Ktlt/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Ktlt ktlt = db.Ktlts.Find(id);
            if (ktlt == null)
            {
                return HttpNotFound();
            }
            return View(ktlt);
        }

        //
        // POST: /Ktlt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ktlt ktlt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ktlt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ktlt);
        }

        //
        // GET: /Ktlt/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Ktlt ktlt = db.Ktlts.Find(id);
            if (ktlt == null)
            {
                return HttpNotFound();
            }
            return View(ktlt);
        }

        //
        // POST: /Ktlt/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ktlt ktlt = db.Ktlts.Find(id);
            db.Ktlts.Remove(ktlt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}