﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class NhapMonController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /NhapMon/

        public ActionResult Index()
        {
            return View(db.NhapMons.ToList());
        }

        //
        // GET: /NhapMon/Details/5

        public ActionResult Details(int id = 0)
        {
            NhapMon nhapmon = db.NhapMons.Find(id);
            if (nhapmon == null)
            {
                return HttpNotFound();
            }
            return View(nhapmon);
        }

        //
        // GET: /NhapMon/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /NhapMon/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NhapMon nhapmon)
        {
            if (ModelState.IsValid)
            {
                db.NhapMons.Add(nhapmon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nhapmon);
        }

        //
        // GET: /NhapMon/Edit/5

        public ActionResult Edit(int id = 0)
        {
            NhapMon nhapmon = db.NhapMons.Find(id);
            if (nhapmon == null)
            {
                return HttpNotFound();
            }
            return View(nhapmon);
        }

        //
        // POST: /NhapMon/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NhapMon nhapmon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nhapmon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nhapmon);
        }

        //
        // GET: /NhapMon/Delete/5

        public ActionResult Delete(int id = 0)
        {
            NhapMon nhapmon = db.NhapMons.Find(id);
            if (nhapmon == null)
            {
                return HttpNotFound();
            }
            return View(nhapmon);
        }

        //
        // POST: /NhapMon/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NhapMon nhapmon = db.NhapMons.Find(id);
            db.NhapMons.Remove(nhapmon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}