﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class NmnController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Nmn/

        public ActionResult Index()
        {
            return View(db.Nmns.ToList());
        }

        //
        // GET: /Nmn/Details/5

        public ActionResult Details(int id = 0)
        {
            Nmn nmn = db.Nmns.Find(id);
            if (nmn == null)
            {
                return HttpNotFound();
            }
            return View(nmn);
        }

        //
        // GET: /Nmn/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Nmn/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Nmn nmn)
        {
            if (ModelState.IsValid)
            {
                db.Nmns.Add(nmn);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nmn);
        }

        //
        // GET: /Nmn/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Nmn nmn = db.Nmns.Find(id);
            if (nmn == null)
            {
                return HttpNotFound();
            }
            return View(nmn);
        }

        //
        // POST: /Nmn/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Nmn nmn)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nmn).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nmn);
        }

        //
        // GET: /Nmn/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Nmn nmn = db.Nmns.Find(id);
            if (nmn == null)
            {
                return HttpNotFound();
            }
            return View(nmn);
        }

        //
        // POST: /Nmn/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Nmn nmn = db.Nmns.Find(id);
            db.Nmns.Remove(nmn);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}