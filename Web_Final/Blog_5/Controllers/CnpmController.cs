﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class CnpmController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Cnpm/

        public ActionResult Index()
        {
            return View(db.Cnpms.ToList());
        }

        //
        // GET: /Cnpm/Details/5

        public ActionResult Details(int id = 0)
        {
            Cnpm cnpm = db.Cnpms.Find(id);
            if (cnpm == null)
            {
                return HttpNotFound();
            }
            return View(cnpm);
        }

        //
        // GET: /Cnpm/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Cnpm/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cnpm cnpm)
        {
            if (ModelState.IsValid)
            {
                db.Cnpms.Add(cnpm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cnpm);
        }

        //
        // GET: /Cnpm/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Cnpm cnpm = db.Cnpms.Find(id);
            if (cnpm == null)
            {
                return HttpNotFound();
            }
            return View(cnpm);
        }

        //
        // POST: /Cnpm/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cnpm cnpm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cnpm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cnpm);
        }

        //
        // GET: /Cnpm/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Cnpm cnpm = db.Cnpms.Find(id);
            if (cnpm == null)
            {
                return HttpNotFound();
            }
            return View(cnpm);
        }

        //
        // POST: /Cnpm/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cnpm cnpm = db.Cnpms.Find(id);
            db.Cnpms.Remove(cnpm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}