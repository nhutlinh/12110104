﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class HtttController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Httt/

        public ActionResult Index()
        {
            return View(db.Httts.ToList());
        }

        //
        // GET: /Httt/Details/5

        public ActionResult Details(int id = 0)
        {
            Httt httt = db.Httts.Find(id);
            if (httt == null)
            {
                return HttpNotFound();
            }
            return View(httt);
        }

        //
        // GET: /Httt/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Httt/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Httt httt)
        {
            if (ModelState.IsValid)
            {
                db.Httts.Add(httt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(httt);
        }

        //
        // GET: /Httt/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Httt httt = db.Httts.Find(id);
            if (httt == null)
            {
                return HttpNotFound();
            }
            return View(httt);
        }

        //
        // POST: /Httt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Httt httt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(httt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(httt);
        }

        //
        // GET: /Httt/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Httt httt = db.Httts.Find(id);
            if (httt == null)
            {
                return HttpNotFound();
            }
            return View(httt);
        }

        //
        // POST: /Httt/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Httt httt = db.Httts.Find(id);
            db.Httts.Remove(httt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}