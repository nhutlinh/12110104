﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class LtwController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Ltw/

        public ActionResult Index()
        {
            return View(db.Ltws.ToList());
        }

        //
        // GET: /Ltw/Details/5

        public ActionResult Details(int id = 0)
        {
            Ltw ltw = db.Ltws.Find(id);
            if (ltw == null)
            {
                return HttpNotFound();
            }
            return View(ltw);
        }

        //
        // GET: /Ltw/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Ltw/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ltw ltw)
        {
            if (ModelState.IsValid)
            {
                db.Ltws.Add(ltw);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ltw);
        }

        //
        // GET: /Ltw/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Ltw ltw = db.Ltws.Find(id);
            if (ltw == null)
            {
                return HttpNotFound();
            }
            return View(ltw);
        }

        //
        // POST: /Ltw/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ltw ltw)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ltw).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ltw);
        }

        //
        // GET: /Ltw/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Ltw ltw = db.Ltws.Find(id);
            if (ltw == null)
            {
                return HttpNotFound();
            }
            return View(ltw);
        }

        //
        // POST: /Ltw/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ltw ltw = db.Ltws.Find(id);
            db.Ltws.Remove(ltw);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}