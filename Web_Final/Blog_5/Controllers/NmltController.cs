﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class NmltController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Nmlt/

        public ActionResult Index()
        {
            var nmlts = db.Nmlts.Include(n => n.UserProfile);
            return View(nmlts.ToList());
        }

        //
        // GET: /Nmlt/Details/5

        public ActionResult Details(int id = 0)
        {
            Nmlt nmlt = db.Nmlts.Find(id);
            if (nmlt == null)
            {
                return HttpNotFound();
            }
            return View(nmlt);
        }

        //
        // GET: /Nmlt/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Nmlt/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Nmlt nmlt)
        {
            if (ModelState.IsValid)
            {
                db.Nmlts.Add(nmlt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", nmlt.UserProfileUserId);
            return View(nmlt);
        }

        //
        // GET: /Nmlt/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Nmlt nmlt = db.Nmlts.Find(id);
            if (nmlt == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", nmlt.UserProfileUserId);
            return View(nmlt);
        }

        //
        // POST: /Nmlt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Nmlt nmlt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nmlt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", nmlt.UserProfileUserId);
            return View(nmlt);
        }

        //
        // GET: /Nmlt/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Nmlt nmlt = db.Nmlts.Find(id);
            if (nmlt == null)
            {
                return HttpNotFound();
            }
            return View(nmlt);
        }

        //
        // POST: /Nmlt/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Nmlt nmlt = db.Nmlts.Find(id);
            db.Nmlts.Remove(nmlt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}