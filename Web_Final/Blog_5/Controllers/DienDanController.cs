﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5.Models;

namespace Blog_5.Controllers
{
    public class DienDanController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /DienDan/

        public ActionResult Index()
        {
            return View(db.DienDans.ToList());
        }

        //
        // GET: /DienDan/Details/5

        public ActionResult Details(int id = 0)
        {
            DienDan diendan = db.DienDans.Find(id);
            if (diendan == null)
            {
                return HttpNotFound();
            }
            return View(diendan);
        }

        //
        // GET: /DienDan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DienDan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DienDan diendan)
        {
            if (ModelState.IsValid)
            {
                db.DienDans.Add(diendan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(diendan);
        }

        //
        // GET: /DienDan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DienDan diendan = db.DienDans.Find(id);
            if (diendan == null)
            {
                return HttpNotFound();
            }
            return View(diendan);
        }

        //
        // POST: /DienDan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DienDan diendan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(diendan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(diendan);
        }

        //
        // GET: /DienDan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DienDan diendan = db.DienDans.Find(id);
            if (diendan == null)
            {
                return HttpNotFound();
            }
            return View(diendan);
        }

        //
        // POST: /DienDan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DienDan diendan = db.DienDans.Find(id);
            db.DienDans.Remove(diendan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}